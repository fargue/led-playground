#!/usr/bin/env python3
# rpi_ws281x library strandtest example
# Author: Tony DiCola (tony@tonydicola.com)
#
# Direct port of the Arduino NeoPixel library strandtest example.  Showcases
# various animations on a strip of NeoPixels.

import time
from rpi_ws281x import *
import argparse
import random

# LED strip configuration:
LED_COUNT      = 100    # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 200     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

COLOR_RED    = Color(255,0,0)
COLOR_GREEN  = Color(0,128,0)
COLOR_WHITE  = Color(255,255,255)
COLOR_ORANGE = Color(255,128,0)
COLOR_YELLOW = Color(255,255,0)
COLOR_OFF    = Color(0,0,0)


# Define functions which animate LEDs in various ways.
def random_color():
    return Color(random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))


def color_wipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms/1000.0)


def run_to(strip, end, color=COLOR_WHITE):
    """Wipe color across display a pixel at a time."""
    for i in range(200,end):
        strip.setPixelColor(i, color)
        strip.show()
        strip.setPixelColor(i-1, COLOR_OFF)
        strip.show()
        time.sleep(1/1000000.0)


def blast(strip,start,size=10):
    wait_ms=20
    strip.setPixelColor(start, COLOR_RED)
    strip.show()
    time.sleep(wait_ms/10000.0)
    for x in range(4):
        for i in range(size):
            strip.setPixelColor(start+i, COLOR_RED)
            strip.setPixelColor(start-i, COLOR_RED)
            strip.show()
        for i in reversed(range(size)):
            strip.setPixelColor(start+i, COLOR_OFF)
            strip.setPixelColor(start-i, COLOR_OFF)
            strip.show()
        for i in range(5):
            strip.setPixelColor(start+i, COLOR_ORANGE)
            strip.setPixelColor(start-i, COLOR_ORANGE)
            strip.show()
        for i in reversed(range(5)):
            strip.setPixelColor(start+i, COLOR_OFF)
            strip.setPixelColor(start-i, COLOR_OFF)
            strip.show()
        time.sleep(1/100000.0)

def intersect(strip, color_up, color_down, wait_ms=50):
    for i in range(strip.numPixels()):
        down_px = strip.numPixels() - i
        if (i % 2) == 0:
            strip.setPixelColor(i, color_up)
        else:
            strip.setPixelColor(down_px, color_down)
        strip.show()
        time.sleep(wait_ms/1000.0)

def theaterChase(strip, color, wait_ms=50, iterations=10):
    """Movie theater light style chaser animation."""
    for j in range(iterations):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, color)
            strip.show()
            time.sleep(wait_ms/1000.0)
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, 0)

def wheel(pos):
    """Generate rainbow colors across 0-255 positions."""
    if pos < 85:
        return Color(pos * 3, 255 - pos * 3, 0)
    elif pos < 170:
        pos -= 85
        return Color(255 - pos * 3, 0, pos * 3)
    else:
        pos -= 170
        return Color(0, pos * 3, 255 - pos * 3)

def rainbow(strip, wait_ms=20, iterations=1):
    """Draw rainbow that fades across all pixels at once."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((i+j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)

def rainbowCycle(strip, wait_ms=20, iterations=5):
    """Draw rainbow that uniformly distributes itself across all pixels."""
    for j in range(256*iterations):
        for i in range(strip.numPixels()):
            strip.setPixelColor(i, wheel((int(i * 256 / strip.numPixels()) + j) & 255))
        strip.show()
        time.sleep(wait_ms/1000.0)

def theaterChaseRainbow(strip, wait_ms=50):
    """Rainbow movie theater light style chaser animation."""
    for j in range(256):
        for q in range(3):
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, wheel((i+j) % 255))
            strip.show()
            time.sleep(wait_ms/1000.0)
            for i in range(0, strip.numPixels(), 3):
                strip.setPixelColor(i+q, 0)

# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true', help='clear the display on exit')
    parser.add_argument('-d', '--delay', type=int,
                        help='delay between operations in mSecD')
    parser.add_argument('-n', '--number_of_leds', type=int,
                        help='Number of LEDs in strand')
    parser.add_argument('-b', '--brightness', type=int,
                        help='led brightness. Set to 0 for darkest and 255 for brightest')
    args = parser.parse_args()

    if not args.delay:
        args.delay = 50

    if not args.number_of_leds:
        args.number_of_leds = LED_COUNT

    if args.brightness is not None:
        if args.brightness < 0 or args.brightness > 255:
            print('Invalid brightness, must be between 0 and 255')
            raise ValueError
        else:
            print('Using brightness value of %d' % (args.brightness))
    else:
        print('setting brightness to default %d' % (LED_BRIGHTNESS))
        args.brightness = LED_BRIGHTNESS

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(
        args.number_of_leds, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, args.brightness, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print ('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:

        while True:
            #print ('Color wipe animations.')
            #color_wipe(strip, random_color(), args.delay)
            #print ('Theater chase animations.')
            #theaterChase(strip, random_color(), args.delay)
            #print ('Rainbow animations.')
            #rainbow(strip, args.delay)
            #print('RainbowCycle')
            #rainbowCycle(strip, args.delay)
            #print('TheaterChaseRainbow')
            #theaterChaseRainbow(strip, args.delay)
            #print ('Intersect')
            #intersect(strip, random_color(), random_color(), args.delay)
            height= random.randint(240,290)
            run_to(strip, height)
            blast(strip,height)
    except KeyboardInterrupt:
        if args.clear:
            color_wipe(strip, Color(0,0,0), 10)
