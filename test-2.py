import time
from rpi_ws281x import *
import argparse

# LED strip configuration:
LED_COUNT = 100    # Number of LED pixels.
LED_PIN = 18      # GPIO pin connected to the pixels (18 uses PWM!).
#LED_PIN        = 10      # GPIO pin connected to the pixels (10 uses SPI /dev/spidev0.0).
LED_FREQ_HZ = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 100     # Set to 0 for darkest and 255 for brightest
# True to invert the signal (when using NPN transistor level shift)
LED_INVERT = False
LED_CHANNEL = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53


def colorWipe(strip, color, wait_ms=50):
    """Wipe color across display a pixel at a time."""
    for i in range(strip.numPixels()):
        strip.setPixelColor(i, color)
        strip.show()
        time.sleep(wait_ms/1000.0)

def up_and_down(strip,color, wait_ms=50):
    for i in range(strip.numPixels()):
        if (i % 2) == 0:
            strip.setPixelColor(i, color)
            strip.show()
            time.sleep(wait_ms/1000.0)
    for i in range(strip.numPixels()):
        if (i % 2) != 0:
            strip.setPixelColor(i, color)
            strip.show()
            time.sleep(wait_ms/1000.0)
    for i in reversed(range(strip.numPixels())):
        if (i % 2) == 0:
            strip.setPixelColor(i, Color(0,0,0))
            strip.show()
            time.sleep(wait_ms/1000.0)
    for i in reversed(range(strip.numPixels())):
        if (i % 2) != 0:
            strip.setPixelColor(i, Color(0,0,0))
            strip.show()
            time.sleep(wait_ms/1000.0)

# Main program logic follows:
if __name__ == '__main__':
    # Process arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('-c', '--clear', action='store_true',
                        help='clear the display on exit')
    parser.add_argument('-d', '--delay', type=int,
                        help='delay between operations in mSecD')
    parser.add_argument('-b', '--brightness', type=int,
                        help='led brightness. Set to 0 for darkest and 255 for brightest')
    args = parser.parse_args()

    if not args.delay:
        args.delay = 50

    if args.brightness is not None:
        if args.brightness < 0 or args.brightness > 255:
            print('Invalid brightness, must be between 0 and 255')
            raise ValueError
        else:
            print('Using brightness value of %d' % (args.brightness))
    else:
        print('setting brightness to default %d' % (LED_BRIGHTNESS))
        args.brightness = LED_BRIGHTNESS

    # Create NeoPixel object with appropriate configuration.
    strip = Adafruit_NeoPixel(
        LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, args.brightness, LED_CHANNEL)
    # Intialize the library (must be called once before other functions).
    strip.begin()

    print('Press Ctrl-C to quit.')
    if not args.clear:
        print('Use "-c" argument to clear LEDs on exit')

    try:
        while True:
            print('Here we go!')
            up_and_down(strip, Color(255, 0, 0),args.delay)
            up_and_down(strip, Color(255, 255, 0),args.delay)
            up_and_down(strip, Color(0, 255, 0), args.delay)
            up_and_down(strip, Color(0, 255, 255), args.delay)
            up_and_down(strip, Color(0, 0, 255), args.delay)
            up_and_down(strip, Color(255, 0, 255), args.delay)
    except KeyboardInterrupt:
        if args.clear:
            colorWipe(strip, Color(0, 0, 0), 10)
